module github.com/bshore/steggo

go 1.17

require (
	github.com/spf13/cobra v1.3.0
	golang.org/x/image v0.0.0-20190802002840-cff245a6509b
)

require (
	github.com/inconshreveable/mousetrap v1.0.0 // indirect
	github.com/spf13/pflag v1.0.5 // indirect
)
